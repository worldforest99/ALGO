package com.ssafy.algo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

//냉장고
public class Main_JO_1828 {
	static int[][] reTemp;

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int N = Integer.parseInt(in.readLine());// 냉장고 개수
		reTemp = new int[N][2];
		int max = -270;
		int min = 10000;
		for (int n = 0; n < N; n++) {
			StringTokenizer st = new StringTokenizer(in.readLine());
			reTemp[n][0] = Integer.parseInt(st.nextToken());// 최저온도
			reTemp[n][1] = Integer.parseInt(st.nextToken());// 최고온도
//			System.out.println(reTemp[n][0]+" "+reTemp[n][1]);
//			max = max >= reTemp[n][1] ? max : reTemp[n][1];
//			min = min <= reTemp[n][1] ? min : reTemp[n][0];
		}
		for (int i = 0; i < reTemp.length; i++) {
			System.out.println(reTemp[i][0] + " " + reTemp[i][1]);
		}
		System.out.println(max + " " + min);
	}
}
