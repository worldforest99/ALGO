package com.ssafy.algo;

import java.util.Scanner;

public class Solution_D3_8016 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for (int t = 1; t <= T; t++) {
			int N = sc.nextInt();
			long left = 1L;
			long right = 1L;
			for (int i = 2; i <= N; i++) {
				right += (2 * i - 1) * 2;
				left += 2 * (2 * (i - 1) - 1);
			}
			System.out.println("#" + t + " " + left + " " + right);
		}
	}
}
