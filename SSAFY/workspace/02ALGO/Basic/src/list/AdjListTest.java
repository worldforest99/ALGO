package list;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class AdjListTest {

	static class Node {
		int to;// 상대 정점 번호
		Node link;

		public Node(int to) {
			super();
			this.to = to;
		}

		public Node(int to, Node link) {
			super();
			this.to = to;
			this.link = link;
		}

	}

	private static Node[] adjlist;
	private static int N;
	private static boolean[] visited;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();// 정점 수
		int M = sc.nextInt(); // 간선 수

		adjlist = new Node[N + 1];
		visited = new boolean[N + 1];

		for (int i = 0; i < M; i++) {
			int from = sc.nextInt();
			int to = sc.nextInt();

			adjlist[from] = new Node(to, adjlist[from]);
			adjlist[to] = new Node(from, adjlist[to]);
		}

		//dfs(1);
		bfs(1);
	}

	private static void dfs(int cur) {
		visited[cur] = true;
		System.out.print((char) (cur + 64) + " ");
		for (Node temp = adjlist[cur]; temp != null; temp = temp.link) {
			if (!visited[temp.to])// 인접 여부 확인하지 않아!
				dfs(temp.to);
		}
	}

	private static void bfs(int cur) {

		Queue<Integer> queue = new LinkedList<Integer>();
		visited[cur] = true;
		queue.offer(cur);

		while (!queue.isEmpty()) {
			int current = queue.poll();
			System.out.print((char) (current + 64) + " ");
			
			for (Node temp = adjlist[cur]; temp != null; temp = temp.link) {
				if (!visited[temp.to]) {// 인접 여부 확인하지 않아!
					visited[temp.to] = true;
					queue.offer(temp.to);
				}

			}
		}
	}
}
