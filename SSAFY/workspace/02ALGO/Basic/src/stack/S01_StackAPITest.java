package stack;

import java.util.Stack;

public class S01_StackAPITest {

	public static void main(String[] args) {
		Stack<String> stack = new Stack<String>();
		stack.push("serim");
		stack.push("jungmin");
		stack.push("aera");

		System.out.println(stack.size());// 3
		System.out.println(stack.pop());// aera
		System.out.println(stack.size());// 2
		System.out.println(stack.peek());// jungmin
		System.out.println(stack.size());// 2
		System.out.println(stack.pop());// jungmin
		System.out.println(stack.pop());// serim
		System.out.println(stack.isEmpty());// true
	}

}
