package book;

import java.util.Scanner;

public class 미로찾기 {
	// 우 좌 하 상
	static int[] dr = { 0, 0, 1, -1 };
	static int[] dc = { 1, -1, 0, 0 };

	static int R,C, min; // dfs로 할거니까 최소 저장할 min
	static char[][] map;
	static int SR, SC, GR, GC;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		R = sc.nextInt();
		C = sc.nextInt();
		map = new char[R][C];
		// 공백을 붙이나 띄우나?
		for (int r = 0; r < R; r++) {
			map[r] = sc.next().toCharArray();
			for (int c = 0; c < C; c++) {
				// 시작점
				if (map[r][c] == 'S') {
					SR = r;
					SC = c;
				}
				// 도착점
				if (map[r][c] == 'G') {
					GR = r;
					GC = c;
				}
			}
		}
		min = Integer.MAX_VALUE;
		dfs(0, 0, 1);
		System.out.println(min);
	}

	private static void dfs(int r, int c, int count) {
		if (r == GR && c == GC) {
			min = Math.min(min, count);
			return;
		}
		map[r][c] = 3;
		for (int d = 0; d < 4; d++) {
			int nr = r + dr[d];
			int nc = c + dc[d];
			if (!check(nr, nc)) {
				continue;
			}
			if (map[nr][nc] == '#') {
				continue;
			}
			if (map[nr][nc] == '.') {
				dfs(nr, nc, count + 1);
			}
		}
		//
		map[r][c] = 1;
	}

	private static boolean check(int nr, int nc) {
		if (nr < 0 || nc < 0 || nr >= R || nc >= C)// 범위 벗어나면
			return false;
		return true;
	}
}
