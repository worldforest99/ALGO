package book;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class 미로4_BFS {

	static int R, C;
	static char[][] map;
	static int[][] visited;
	static int sr, sc, er, ec;
	static int[] dr = { -1, 1, 0, 0 };
	static int[] dc = { 0, 0, -1, 1 };

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		R = scanner.nextInt();
		C = scanner.nextInt();
		map = new char[R][C];
		visited = new int[R][C];

		for (int r = 0; r < R; r++) {
			map[r] = scanner.next().toCharArray();
		}
		for (int r = 0; r < R; r++) {
			for (int c = 0; c < C; c++) {
				if (map[r][c] == 'S') {
					sr = r;
					sc = c;
				} else if (map[r][c] == 'G') {
					er = r;
					ec = c;
				}
			}
		}
		Queue<int[]> que = new LinkedList<>();
		que.offer(new int[] { sr, sc });
		visited[sr][sc] = 0;// distance
		while (!que.isEmpty()) {
			int[] curr = que.poll();
			int cr = curr[0];
			int cc = curr[1];
			if (cc == ec && cr == er) {
				break;
			}
			for (int d = 0; d < 4; d++) {
				int nr = cr + dr[d];
				int nc = cc + dc[d];
				if (!check(nr, nc)) {
					continue;
				}
				if (visited[nr][nc] == 1) {
					continue;
				}
				if (map[nr][nc] == '.' || map[nr][nc] == 'G') {
					visited[nr][nc] = visited[cr][cc] + 1;
					que.offer(new int[] { nr, nc });
				}
			}
		}
		System.out.println(visited[er][ec]);
		print(visited);
	}

	private static boolean check(int nr, int nc) {
		if (nr >= 0 && nr < R && nc >= 0 && nc < C) {
			return true;
		}
		return false;
	}

	public static void print(int[][] m) {
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length; j++) {
				System.out.print(m[i][j] + " ");
			}
			System.out.println();
		}
	}

}