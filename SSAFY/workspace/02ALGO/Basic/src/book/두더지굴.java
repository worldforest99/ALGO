package book;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

public class 두더지굴 {
	static int MAX_SIZE = 20;
	static int N; // 행과 열
	// 우 좌 하 상
	static int[] dr = { 0, 0, 1, -1 };
	static int[] dc = { 1, -1, 0, 0 };
	static int zipnum; // 집 번호
	static int[] zips;// 각 단지별 집 수

	static int[][] map; // 두더지굴 현황

	static boolean[][] check;// 방문했나?

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		N = sc.nextInt();
		map = new int[N][N];
		zips = new int[N * N];

		// 두더지굴 그리기
		for (int r = 0; r < N; r++) {
			for (int c = 0; c < N; c++) {
				map[r][c] = sc.nextInt();
			}
		}
		zipnum = 1;
		// 두더지굴 찾기
		for (int r = 0; r < N; r++) {
			for (int c = 0; c < N; c++) {
				// 1이면 방문하지 않은거야
				if (map[r][c] == 1 && map[r][c] == 1) {
					zipnum++;
//					bfs(r, c);
//					dfs1(r, c);
					dfs2(r, c);
				}
			}
		}
		System.out.println(zipnum - 1); // 총 몇개야?

		// Integer로 해야되나봐 collections는
//		Arrays.sort(zips,Collections.reverseOrder());

		Arrays.sort(zips, 2, zipnum + 1);
		for (int i = zipnum; i >= 2; i--) {
			System.out.println(zips[i]);
		}

		sc.close();
	}

	// stack으로
	private static void dfs2(int r, int c) {
		Stack<RC> stack = new Stack<RC>();
		stack.add(new RC(r, c));

		while (!stack.isEmpty()) {
			RC curr = stack.pop();
			int cr = curr.r;
			int cc = curr.c;
			for (int d = 0; d < 4; d++) {
				int nr = cr + dr[d];
				int nc = cc + dc[d];

				if (!check(nr, nc)) {
					continue;
				}
				if (map[nr][nc] == 1) {
					zips[zipnum]++;
					map[nr][nc] = zipnum;// 방문표시
					// 여기 모르겠다
					stack.add(new RC(cr, cc));
					stack.add(new RC(nr, nc));
				}
			}
		}
	}

	// 재귀함수로 dfs
	private static void dfs1(int r, int c) {
		// boolean[][] check 대신 사용하는거야
		map[r][c] = zipnum;
		zips[zipnum]++; // zipnum의 집 개수 증가

		for (int d = 0; d < 4; d++) {
			int nr = r + dr[d];
			int nc = c + dc[d];
			if (!check(nr, nc)) {
				continue;
			}
			if (map[nr][nc] == 1) {
				dfs1(nr, nc);
			}
		}

	}

	private static void bfs(int r, int c) {

		Queue<RC> queue = new LinkedList<RC>();
		queue.add(new RC(r, c));

		map[r][c] = zipnum;
		zips[zipnum]++;// zipnum의 집 수 더하기

		while (!queue.isEmpty()) {// 큐 없을때까지
			// 현재 큐에 있는 r,c 뽑아서
			RC curr = queue.poll();
			int cr = curr.r;
			int cc = curr.c;

			for (int d = 0; d < 4; d++) {// 사방 탐색 시작
				// 큐에서 뽑은 좌표에 사방 탐색하면서
				int nr = cr + dr[d];
				int nc = cc + dc[d];
				if (!check(nr, nc)) {// 범위 벗어나면
					continue;
				}
				if (map[nr][nc] == 0) {
					continue;
				}
				if (map[nr][nc] == 1) {
					map[nr][nc] = zipnum; // 방문표시 대신
					// * 여기 빼먹었다
					zips[zipnum]++;
					queue.add(new RC(nr, nc));
				}
			}
		}
//		System.out.println(zipnum+"번 집은 "+zips[zipnum]);

	}

	// 범위 확인
	private static boolean check(int nr, int nc) {
		// 범위 벗어나면 false
		if (nr < 0 || nc < 0 || nr >= N || nc >= N) {
			return false;
		}
		return true;
	}

	// 좌표 클래스
	static class RC {
		int r;
		int c;

		RC(int _r, int _c) {
			this.r = _r;
			this.c = _c;
		}
	}
}
