package tree;

import java.util.LinkedList;
import java.util.Queue;

public class BinaryTree {

	private Object[] nodes;
	private int lastIndex;
	private final int SIZE;

	public BinaryTree(int size) {
		nodes = new Object[size + 1];
		this.SIZE = size;
	}

	public boolean isEmpty() {
		return lastIndex == 0;
	}

	public boolean isFull() {
		return lastIndex == SIZE;
	}

	public void add(Object e) {
		if (isFull()) {
			System.out.println("포화상태");
			return;
		}
		nodes[++lastIndex] = e;
	}

	public void searchPreOrder() {
		searchPreOrder(1);
		System.out.println();
	}

	// V 방문
	private void searchPreOrder(int index) {
		if (index <= lastIndex) {
			System.out.print(nodes[index] + " "); // V
			searchPreOrder(index * 2);// L
			searchPreOrder(index * 2 + 1);// R
		}
	}

	public void searchInOrder() {
		searchInOrder(1);
		System.out.println();
	}

	public void searchInOrder(int index) {
		if (index <= lastIndex) {
			searchInOrder(index * 2);// L
			System.out.print(nodes[index] + " "); // V
			searchInOrder(index * 2 + 1);// R
		}
	}

	public void searchPostOrder() {
		searchPostOrder(1);
		System.out.println();
	}

	public void searchPostOrder(int index) {
		if (index <= lastIndex) {
			searchPostOrder(index * 2);// L
			searchPostOrder(index * 2 + 1);// R
			System.out.print(nodes[index] + " "); // V
		}
	}

	public void searchBFS2() {

		if (isEmpty())
			return;

		Queue<Integer> queue = new LinkedList<Integer>();
		queue.offer(1);

		while (!queue.isEmpty()) {
			int cur = queue.poll();
			System.out.print(nodes[cur] + " ");
			if (cur * 2 <= lastIndex)
				queue.offer(cur * 2);// L
			if (cur * 2 + 1 <= lastIndex)
				queue.offer(cur * 2 + 1);// R
		}
		System.out.println();
	}

	public void searchBFS() {

		if (isEmpty())
			return;

		Queue<Integer> queue = new LinkedList<Integer>();
		queue.offer(1);

		while (!queue.isEmpty()) {
			int size = queue.size();
			// level 단위로 출력가능
			while (size-- > 0) {
				int cur = queue.poll();
				System.out.print(nodes[cur] + " ");
				if (cur * 2 <= lastIndex)
					queue.offer(cur * 2);// L
				if (cur * 2 + 1 <= lastIndex)
					queue.offer(cur * 2 + 1);// R
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {
		BinaryTree tree = new BinaryTree(10);
		for (int i = 0; i < 10; i++) {
			tree.add((char) ('A' + i));
		}

		tree.searchBFS();
		tree.searchPreOrder();
		tree.searchInOrder();
		tree.searchPostOrder();

	}

}
