package Queue;

import java.util.Arrays;
import java.util.Comparator;

public class Q03_ComparableTest {

	static class Student implements Comparable<Student> {
		int no, score;

		public Student(int no, int score) {
			super();
			this.no = no;
			this.score = score;
		}

		@Override
		public int compareTo(Student o) {

			// 번호가 음수 없다 생각하고한다
			return this.no - o.no;
		}

		@Override
		public String toString() {
			return "Student [no=" + no + ", score=" + score + "]";
		}
	}

	public static void main(String[] args) {
		Student[] students = new Student[] { new Student(1, 100), new Student(10, 50), new Student(5, 10)

		};
		Arrays.sort(students);
		for (Student student : students) {
			System.out.println(student);
		}
		Arrays.sort(students, new Comparator<Student>() {

			@Override
			public int compare(Student o1, Student o2) {
				// 오름차순
				return o1.score - o2.score;

				// 오름차순
//				return o2.score-o1.score;

			}

		});
	}

}
