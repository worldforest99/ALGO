package Queue;

import java.util.Comparator;
import java.util.PriorityQueue;

import Queue.Q03_ComparableTest.Student;

//1812번
public class Q04_PriorityQueueTest {

	static class Student implements Comparable<Student> {
		int no, score;

		public Student(int no, int score) {
			super();
			this.no = no;
			this.score = score;
		}

		@Override
		public int compareTo(Student o) {

			// 번호가 음수 없다 생각하고한다
			return this.no - o.no;
		}

		@Override
		public String toString() {
			return "Student [no=" + no + ", score=" + score + "]";
		}
	}

	public static void main(String[] args) {
//		PriorityQueue<Student> queue = new PriorityQueue<Student>();
		PriorityQueue<Student> queue = new PriorityQueue<Student>(new Comparator<Student>() {

			// 점수 순서대로 나오게
			@Override
			public int compare(Student o1, Student o2) {
				// 오름차순
				return o1.score - o2.score;

				// 오름차순
//				return o2.score-o1.score;

			}

		});
		queue.offer(new Student(10, 100));
		queue.offer(new Student(1, 80));
		queue.offer(new Student(5, 50));

		System.out.println(queue.poll());
		System.out.println(queue.poll());
		System.out.println(queue.poll());
	}

}
