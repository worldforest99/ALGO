package perm;

import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

public class 순열01 {
	static int N, K;// 주어진 경우의 수, 선택 해야하는 경우의
	static int answer;
	static int[] input;
	static int[] result;
	static boolean[] visited;

	public static void main(String[] args) {
		Scanner sc = new Scanner(new InputStreamReader(System.in));
		// N개중에 K개 선택할 때
		N = Integer.parseInt(sc.next());
		K = Integer.parseInt(sc.next());
		input = new int[N];
		result = new int[K];
		visited = new boolean[N];
		for (int i = 0; i < N; i++) {
			input[i] = sc.nextInt();
		}
		Arrays.sort(input);
		permutation(0);
	}

	private static void permutation(int k) {
		if (k == K) {// 원하는 개수 만큼 뽑으면
			for (int i = 0; i < result.length; i++) {
				
				System.out.print(result[i]+ " ");
			}
			System.out.println();
			return;
		}
		for (int i = 0; i < N; i++) {
			if (visited[i] != true) {
				visited[i] = true;
				result[k] = input[i];
				permutation(k + 1);
				visited[i] = false;
			}
		}
	}
}
