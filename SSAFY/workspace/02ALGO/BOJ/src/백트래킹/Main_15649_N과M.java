package 백트래킹;

import java.util.Scanner;

// 순열
public class Main_15649_N과M {
	static int N, M;
	static boolean[] check;
	static int[] answer;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();
		M = sc.nextInt();
		check = new boolean[N];
		answer = new int[M];
		permutation(0, 0);
	}

	private static void permutation(int num, int count) {
		if (count == M) {// 뽑은 개수
			for (int i = 0; i < M; i++) {
				System.out.print((answer[i] + 1) + " ");
			}
			System.out.println();
			return;
		}

		for (int i = 0; i < N; i++) {

			if (!check[i]) {// 뽑지 않았으면
				answer[count] = i;
				check[i] = true;
				permutation(i + 1, count + 1);
				check[i] = false;
			}
		}
	}
}
