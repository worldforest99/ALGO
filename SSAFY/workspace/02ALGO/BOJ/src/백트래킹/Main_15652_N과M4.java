package 백트래킹;

import java.util.Scanner;

public class Main_15652_N과M4 {
	static int N, M;
	static boolean[] check;
	static int[] answer;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();
		M = sc.nextInt();
		check = new boolean[N];
		answer = new int[M];

		combination(0, 0);

	}

	private static void combination(int n, int cnt) {
		if (cnt == M) {
			for (int i = 0; i < M; i++) {
				System.out.print((answer[i] + 1) + " ");
			}
			System.out.println();
			return;
		}
		for (int i = 0; i < N; i++) {
			check[i] = true;
			answer[cnt] = i;
			combination(i, cnt + 1);
			check[i] = false;
		}

	}
}
