package 입출력과_사칙연산;

import java.util.Scanner;

public class 곱셈 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int secondnum[] = new int[3];
		int first = sc.nextInt();
		int second = sc.nextInt();

		for (int i = 2; i >= 0; i--) {
			secondnum[i] = second / (int) Math.pow(10, i);
			second -= secondnum[i] * (int) Math.pow(10, i);
		}
		System.out.println(first * secondnum[0]);
		System.out.println(first * secondnum[1]);
		System.out.println(first * secondnum[2]);
		System.out.println(first * (secondnum[0] + secondnum[1]*10 + secondnum[2]*100));
	}

}
