package DFS와BFS;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

// silver 2
public class DFS와BFS {

	// vertex 정점 edge 간선
	static int N, E, V;// 정점개수, 간선, 탐색 시작할 정점 번호
	static boolean[] visited;
	static ArrayList<Integer>[] vertex;
//	static int[][] edge;
//	static int[] answer;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();
		E = sc.nextInt();
		V = sc.nextInt();
		visited = new boolean[N + 1];
		vertex = new ArrayList[N + 1];

		for (int n = 1; n < N + 1; n++) {
			// 양방향 노드
			vertex[n] = new ArrayList<Integer>();
		}

		for (int i = 0; i < E; i++) {
			int start = sc.nextInt();
			int end = sc.nextInt();
			vertex[start].add(end);
			vertex[end].add(start);
		}

		for (int i = 1; i < N + 1; i++) {
			Collections.sort(vertex[i]);
		}
		
		dfs(V);
		visited = new boolean[N + 1];
		System.out.println();
		bfs(V);
		sc.close();

	}

	private static void bfs(int start) {
		Queue<Integer> que = new LinkedList<Integer>();
		que.add(start);
		visited[start] = true;
		while (!que.isEmpty()) {
			int v = que.poll();
			// 현재 점
			System.out.print(v + " ");
			// 현재 점에 연결된 점
			for (int ver : vertex[v]) {
				if (!visited[ver]) {
					visited[ver] = true;
					que.add(ver);
				}
			}
		}
	}

	private static void dfs(int start) {
		if (visited[start]) {
			return;
		}

		visited[start] = true;
		System.out.print(start + " ");
		for (int ver : vertex[start]) {
			if (!visited[ver]) {
				dfs(ver);
			}
		}
	}

}
