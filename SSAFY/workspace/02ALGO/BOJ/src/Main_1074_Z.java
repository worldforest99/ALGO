import java.util.Scanner;

public class Main_1074_Z {
	static int N, R, C;
	static int answer;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();
		R = sc.nextInt();
		C = sc.nextInt();

		find(N, 0, 0);
		System.out.println(answer);

		sc.close();

	}

	// (R,C)가 어떤 사분면에 위치하는가?
	// r, c 사분면 검사 시작 위치
	private static void find(int n, int r, int c) {
		int check = (int) Math.pow(2, n); // 검사할 범위의 행과 열 크기
		int pre = check / 2;
		if (n == 1) {
			answer += (R - r) * 2 + (C - c);
			return;
		}
		// 0<=r<2^N/2
		if (R >= r && R < (r + check / 2)) {
			// 1
			// 0<=c<2^N/2
			if (C >= c && C < (c + check / 2)) {
				find(n - 1, r, c);
			}
			// 2
			// 2^N/2<=c<2^N
			if (C >= (c + check / 2) && C < (c + check)) {
				answer += pre * pre * 1;
				find(n - 1, r, c + check / 2);
			}
		}
		// 2^N/2<=r<2^N
		if (R >= (r + check / 2) && R < (r + check)) {
			// 3
			// 0<=c<2^N/2
			if (C >= c && C < (c + check / 2)) {
				answer += pre * pre * 2;
				find(n - 1, r + check / 2, c);
			}
			// 4
			// 2^N/2<=c<2^N
			if (C >= (c + check / 2) && C < (c + check)) {
				answer += pre * pre * 3;
				find(n - 1, r + check / 2, c + check / 2);
			}
		}
		n -= 1;
	}

}
