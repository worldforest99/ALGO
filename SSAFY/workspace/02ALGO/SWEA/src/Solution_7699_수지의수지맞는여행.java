import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

//DFS
public class Solution_7699_수지의수지맞는여행 {
	static int R, C;
	static char[][] map;
	static int max;
	static boolean[] alphabet;
	static int[] dr = { 0, 0, 1, -1 };
	static int[] dc = { 1, -1, 0, 0 };

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(in.readLine());
		for (int t = 1; t <= T; t++) {
			StringTokenizer st = new StringTokenizer(in.readLine(), " ");
			R = Integer.parseInt(st.nextToken());
			C = Integer.parseInt(st.nextToken());
			map = new char[R][C];
			alphabet = new boolean[26];
			for (int r = 0; r < R; r++) {
				// 빈 칸 없이 주어지니까
				map[r] = in.readLine().toCharArray();
			}
			max = Integer.MIN_VALUE;
			// A=0 B=1 왔는지 확인
//			alphabet[map[0][0] - 'A'] = true;
			dfs(0, 0, 1);
			System.out.println("#" + t + " " + max);

		}
	}

	private static void dfs(int r, int c, int count) {
		// *
		alphabet[map[r][c] - 'A'] = true;// 방문표시
		for (int d = 0; d < 4; d++) {// 사방탐색하면서
			int nr = r + dr[d];
			int nc = c + dc[d];
			if (!check(nr, nc)) {// 갈수없으면
				continue;
			}
			// 갈 수 있고
			if (!alphabet[map[nr][nc] - 'A']) {// 방문한적 없으면
				dfs(nr, nc, count + 1);
			}
		}
		// 백트래킹
		alphabet[map[r][c] - 'A'] = false;
		if (max < count)
			max = count;
	}

	private static boolean check(int nr, int nc) {
		if (nr >= 0 && nr < R && nc >= 0 && nc < C) {
			return true;
		}
		return false;
	}
}
