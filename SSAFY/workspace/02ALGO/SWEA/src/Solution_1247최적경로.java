import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Solution_1247최적경로 {

	static int N;//고객집의 
	static int min, CX,CY, HX,HY;//min 최소이동거리, CX,CY 회사좌표, HX,HY 집좌
	static int[][] customers;// 고객N명 집의 좌표 
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(in.readLine());
		
		for (int t = 1;t<=T; t++) {
			N = Integer.parseInt(in.readLine());
			customers = new int[N][2];
			min = Integer.MAX_VALUE;
			
			StringTokenizer st = new StringTokenizer(in.readLine()," ");
			CX=Integer.parseInt(st.nextToken());
			CY=Integer.parseInt(st.nextToken());
			HX=Integer.parseInt(st.nextToken());
			HY=Integer.parseInt(st.nextToken());
			
			for (int i = 0; i < args.length; i++) {
				customers[i][0]=Integer.parseInt(st.nextToken());
				customers[i][1]=Integer.parseInt(st.nextToken());
			}
			go(0,0,CX,CY,0);
		System.out.println("#"+t+" "+min);
		}
	}
	//뽑고 있는 자리 count
	private static void go(int count, int visited,int bx, int by, int result) {
		if(result >= min) {//가지치기 : 기존까지 순열로 처리 중인 고객집까지 이동했던 거리가 기존의 min보다 크면
			//더 이상 고객집을 방문해도 이동거리를 커질 수밖에 없으니 가지치
		return;
		}
		if(count==N) {//순열 끝나는 조
			result+=Math.abs(bx-HX)+Math.abs(by-HY);
			if(min>result)
				min=result;
			return;
		}
		for (int i = 0; i < N; i++) {
			if((visited & 1<<i)==0) {//i번째 집이 이미 방문했는지(1인지) 확인
				// visited | (1<<i) : 기존 순열상태에서 i 고객집 추가(방문표시)
				go(count+1,visited | (1<<i), customers[i][0],customers[i][1],
						result+Math.abs(bx-customers[i][0])+Math.abs(by-customers[i][1]));
			}
		}
	}

}
