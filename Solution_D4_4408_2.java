package com.ssafy.algo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

//자기 방으로 돌아가기

public class Solution_D4_4408_2 {
	static int[][] room;// 0 현재 방, 1 가야할 방
	static int[] R;
//	static int time;

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(in.readLine());
		for (int t = 1; t <= T; t++) {
			int N = Integer.parseInt(in.readLine());// 학생의 수
			room = new int[N][2];
			R = new int[201];
			for (int i = 0; i < N; i++) {// 학생 수 만큼
				StringTokenizer st = new StringTokenizer(in.readLine());
				int start = Integer.parseInt(st.nextToken());
				int end = Integer.parseInt(st.nextToken());
				start += start % 2;
				end += end % 2;
				room[i][0] = Integer.min(start / 2, end / 2);// 현재 방
				room[i][1] = Integer.max(start / 2, end / 2);// 이동해야할 방
				for (int j = room[i][0]; j <= room[i][1]; j++) {
					R[j] += 1;
				}
			}
			int time = 0;
			for (int i = 0; i < R.length; i++) {
				time = Math.max(time, R[i]);
			}
			System.out.println("#" + t + " " + time);
		}
	}
}
