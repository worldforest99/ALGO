package algo_0205;

//새샘이의 7-3-5 게임
//교수님
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.StringTokenizer;

public class Solution_D3_5948_Pro2 {

	static int T;
	static ArrayList<Integer> sasam = new ArrayList<>();
	static int[] game;

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(in.readLine());
		for (int t = 1; t <= T; t++) {
			sasam.clear();
			game = new int[7];
			StringTokenizer st = new StringTokenizer(in.readLine());
			for (int i = 0; i < 7; i++) {
				game[i] = Integer.parseInt(st.nextToken());
			}
			int sum = 0;
			for (int i = 0; i < 7; i++) {
				for (int j = i + 1; j < 7; j++) {
					for (int k = j + 1; k < 7; k++) {
						if (i < j && j < k && i < k) {
							sum = game[i] + game[j] + game[k];
							if (!sasam.contains(sum))// 중복아닐 때 추가
								sasam.add(sum);
						}
					}
				}
			}
			sasam.sort(new IC());
			System.out.println("#" + t + " " + sasam.get(4));
		}
	}

	// 안에 있는 class만 static
	public static class IC implements Comparator<Integer> {

		@Override
		public int compare(Integer o1, Integer o2) {
//			if (o1 > o2) {
//				return -1;
//			} else if (o1 < o2) {
//				return 1;
//			} else {
//				return 0;
//			}

			// 1 2 3 4 ==> 1 2 = -1
			//return o1 - o2;// ASC 오름차순
			return -(o1-o2);//DESC 내림차순
		}

	}
}
