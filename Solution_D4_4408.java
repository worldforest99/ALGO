package com.ssafy.algo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

//자기 방으로 돌아가기

public class Solution_D4_4408 {
	static int[][] room;// 0 현재 방, 1 가야할 방
	static int[][] route;//
	static int time;

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(in.readLine());
		for (int t = 1; t <= T; t++) {
			int N = Integer.parseInt(in.readLine());// 학생의 수
			room = new int[N][2];
			route = new int[N][2];
			time = 0;
			for (int i = 0; i < N; i++) {// 학생 수 만큼
				StringTokenizer st = new StringTokenizer(in.readLine());
				room[i][0] = Integer.parseInt(st.nextToken());// 현재 방
				room[i][1] = Integer.parseInt(st.nextToken());// 이동해야할 방
//				System.out.println(room[i][0]+" "+room[i][1]);
				route[i][0] = room[i][0] + (room[i][0]) % 2;// 홀수면 1더해서 짝수로 짝수면 0더해서 짝수로
				route[i][1] = room[i][1] + (room[i][1]) % 2;

			}
			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++) {
					if (i == j) {// 같은 거는 비교안해
						continue;
					} else {
						if ((route[j][0] >= route[i][0] && route[j][1] <= route[i][1])
								|| (route[j][1] >= route[i][0] && route[j][0] <= route[i][1])) {
							time++;
							break;
						}
					}
				}
				if (time == 0 && i == N - 1) {
					time = 1;
					break;
				}
			}
			System.out.println("#" + t + " " + time);
		}
	}
}
