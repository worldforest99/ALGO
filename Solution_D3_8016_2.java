package com.ssafy.algo;

import java.util.Scanner;

public class Solution_D3_8016_2 {
	static int T;
	static long fn, en;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		T = sc.nextInt();
		for (int t = 1; t <= T; t++) {
			int N = sc.nextInt();
			fn = 1L;
			en = 1L;
			long d = 2L;
			for (int i = 1; i < N; i++) {
				fn += d;
				d += 4;
				en += d;
			}
			System.out.println("#" + t + " " + fn + " " + en);
		}
	}
}
