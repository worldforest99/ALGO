package com.ssafy.algo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

import com.sun.org.apache.xml.internal.serializer.utils.StringToIntTable;

public class Solution_D3_8658_Summation {

	public static void main(String[] args) throws NumberFormatException, IOException {
		// TODO Auto-generated method stub
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(in.readLine());
		for (int t = 1; t <= T; t++) {
			String str = in.readLine();
			StringTokenizer st = new StringTokenizer(str);
			int[] num = new int[st.countTokens()];
			// char[] num;
			int i = 0;
			while (st.hasMoreTokens()) {
				num[i] = Integer.parseInt(st.nextToken());
				i++;
			}
			for (int j = 0; j < num.length; j++) {
				int number = num[j];
				int sum = 0;
				while (number / 10 != 0) {
					sum += number % 10;
				}
				num[j] = sum;
			}
			int max = Integer.MIN_VALUE;
			int min = Integer.MAX_VALUE;
			for (int j = 0; j < num.length; j++) {
				max = max < num[j] ? num[j] : max;
				min = min > num[j] ? num[j] : min;
			}
			System.out.println("#" + t + " " + max + " " + min);
		}
	}

}
