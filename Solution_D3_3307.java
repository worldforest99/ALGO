package algo_0205;

import java.util.Arrays;
import java.util.Scanner;

//최장 증가 부분 수열
public class Solution_D3_3307 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for (int t = 1; t <= T; t++) {
			int N = sc.nextInt();
			int[] num = new int[N];
			int[] arr = new int[N];
			Arrays.fill(arr, 1);// 최장 증가 부분 수열 길이 저장 배열 //자기자신 부분 수열 1
			for (int n = 0; n < N; n++) {
				num[n] = sc.nextInt();// 값을 저장하는 배열
			}
			for (int i = N - 2; i >= 0; i--) {// 뒤에서 부터
				int max = 0;
				for (int j = i + 1; j < N; j++) {// index 뒤의 값 중에서
					if (num[i] < num[j]) {
						if (max < arr[j])
							max = arr[j];
					}
				}
				arr[i] += max;
			}
			int max = 0;
			for (int i = 0; i < arr.length; i++) {
				if (max < arr[i])
					max = arr[i];
			}
			System.out.println("#" + t + " " + max);
		}
	}

}
