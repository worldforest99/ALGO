package algo_0205;

//새샘이의 7-3-5 게임
//교수님
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Solution_D3_5948_Pro {

	static int T;
	static ArrayList<Integer> sasam = new ArrayList<>();
	static int[] game;

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(in.readLine());
		for (int t = 1; t <= T; t++) {
			sasam.clear();
			game = new int[7];
			StringTokenizer st = new StringTokenizer(in.readLine());
			for (int i = 0; i < 7; i++) {
				game[i] = Integer.parseInt(st.nextToken());
			}
			int sum = 0;
			for (int i = 0; i < 7; i++) {
				for (int j = i + 1; j < 7; j++) {
					for (int k = j + 1; k < 7; k++) {
						if (i < j && j < k && i < k) {
							sum = game[i] + game[j] + game[k];
							if (!sasam.contains(sum))// 중복아닐 때 추가
								sasam.add(sum);
						}
					}
				}
			}
			// arralist를 배열로 바꿨다가
			int[] nums = new int[sasam.size()];
			for (int i = 0; i < nums.length; i++) {
				nums[i] = sasam.get(i);
			}
			// 정렬 ASC 오름차순 : 작은거부터
			// 정렬 DESC 내림차순 : 큰거부터
			Arrays.sort(nums);// asc

			for (int i = 0; i < nums.length / 2; i++) {
				int tp = nums[i];
				nums[i] = nums[nums.length - 1 - i];
				nums[nums.length - 1 - i] = tp;
			}
			System.out.println("#" + t + " " + nums[4]);
		}
	}
}
